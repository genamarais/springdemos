package com.psybergate.spring.di.demo02.v1.framework;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;

public class ContainerUtil {
	
	public static Properties loadPropertiesFile(String propertiesFileName) {
		try {
			ClassLoader classloader = Thread.currentThread().getContextClassLoader();
			Properties properties = new Properties();
			properties.load(classloader.getResourceAsStream(propertiesFileName));
			return properties;
		} catch ( IOException e ) {
			throw new RuntimeException(e);
		}
	}
	
	public static String beanClassKey(String beanName) {
		return beanName + ".class";
	}
	
	public static Class classForName(String className) {
		try {
			return Class.forName(className);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static Object createInstance(Class<?> clazz) {
		try {
			return clazz.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	public static Object createInstance(Constructor<?> constructor, Object...args) {
		try {
			return constructor.newInstance(args);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}

}
