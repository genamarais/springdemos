package com.psybergate.spring.di.demo02.v1.framework;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

public class Container {

	private Map<String, Object> beans = new HashMap<String, Object>();

	private Properties beansProperties;

	public Container(String propertiesFileName) {
		beansProperties = ContainerUtil.loadPropertiesFile(propertiesFileName);
		bootstrap();
	}

	public Object getBean(String beanName) {
		if (!beans.containsKey(beanName)) {
			throw new RuntimeException("Bean not defined");
		}
		return beans.get(beanName);
	}

	private void bootstrap() {
		for (String beanName : getDefinedBeanNames()) {
			Object bean = createBean(beanName);
			beans.put(beanName, bean);
		}

		for (String beanName : getDefinedBeanNames()) {
			for (String fieldName : getPropertiesToInject(beanName)) {
				String dependecyName = beansProperties.getProperty(beanName + "." + fieldName);
				Object dependecy = beans.get(dependecyName);
				injectField(getBean(beanName), fieldName, dependecy);
			}
		}

	}


	private Object createBean(String beanName) {
		try {
			String className = beansProperties.getProperty(beanName);
			return Class.forName(className).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	private void injectField(Object beanToInject, String fieldName, Object dependecy) {
		try {
			Class<? extends Object> clazz = beanToInject.getClass();
			Field fieldToInject = clazz.getDeclaredField(fieldName);
			fieldToInject.setAccessible(true);
			fieldToInject.set(beanToInject, dependecy);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}
	
	private Set<String> getDefinedBeanNames() {
		return beansProperties.stringPropertyNames().stream().map(k -> k.split("\\.")[0]).collect(Collectors.toSet());
	}

	private Set<String> getPropertiesToInject(String beanName) {
		return beansProperties.stringPropertyNames().stream().filter(s -> s.startsWith(beanName + "."))
				.map(k -> k.split("\\.")[1]).collect(Collectors.toSet());
	}

	public static Properties loadPropertiesFile(String propertiesFileName) {
		try {
			ClassLoader classloader = Thread.currentThread().getContextClassLoader();
			Properties properties = new Properties();
			properties.load(classloader.getResourceAsStream(propertiesFileName));
			return properties;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
