package com.psybergate.spring.di.demo02.v2.client;

import com.psybergate.spring.di.demo02.v2.client.config.BeanConfigurations;
import com.psybergate.spring.di.demo02.v2.client.domain.MessageRenderer;
import com.psybergate.spring.di.demo02.v2.framework.Container;

public class MainClient {

	public static void main(String[] args) {
		Container container = new Container(BeanConfigurations.class);
		MessageRenderer renderer = container.getBean(MessageRenderer.class);
		renderer.render();
	}

}
