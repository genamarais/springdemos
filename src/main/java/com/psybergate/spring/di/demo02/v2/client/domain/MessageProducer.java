package com.psybergate.spring.di.demo02.v2.client.domain;

public interface MessageProducer {

	String getMessage();

}
