package com.psybergate.spring.di.demo02.v1.client;

public class MessageRenderer {

	private MessageProducer producer;
	
	public MessageRenderer() {
	}

	public MessageRenderer(MessageProducer producer) {
		this.producer = producer;
	}

	public void render() {
		System.out.println(producer.getMessage());
	}
	
	public void setProducer(MessageProducer producer) {
		this.producer = producer;
	}

}
