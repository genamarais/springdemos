package com.psybergate.spring.di.demo02.v2.client.domain;

import com.psybergate.spring.di.demo02.v2.framework.annotation.InjectMy;

public class MessageRenderer {
	
	private MessageProducer producer;
	
	@InjectMy
	public MessageRenderer(MessageProducer producer) {
		this.producer = producer;
	}
	
	public void render() {
		System.out.println(producer.getMessage());
	}

}
