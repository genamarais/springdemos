package com.psybergate.spring.di.demo02.v1.client;

public interface MessageProducer {

	String getMessage();

}
