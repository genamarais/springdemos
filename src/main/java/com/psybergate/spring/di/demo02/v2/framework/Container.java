package com.psybergate.spring.di.demo02.v2.framework;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.psybergate.spring.di.demo02.v2.framework.annotation.InjectMy;
import com.psybergate.spring.di.demo02.v2.framework.annotation.MyBean;

public class Container {
	private static Map<Class<?>, Object> beanInstances = new HashMap<>();
	
	public Container(Class<?> configClass) {
		loadConfig(configClass);
	}

	private static void loadConfig(Class<?> configClass) {
		try {

			for (Method method : configClass.getMethods()) {
				if (method.isAnnotationPresent(MyBean.class)) {
					Object bean = method.invoke(configClass.newInstance());
					beanInstances.putIfAbsent(method.getReturnType(), bean);
				}
			}

		} catch (InstantiationException | IllegalAccessException | SecurityException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new RuntimeException(e);
		}

	}

	public <T> T getBean(Class<T> beanType) {

		if (beanInstances.containsKey(beanType)) {
			return (T) beanInstances.get(beanType);
		} else {
			T myBean = createBeanInstance(beanType);
			injectSetters(myBean);
			injectFields(myBean);
			beanInstances.put(beanType, myBean);
			return myBean;
		}
	}

	private void injectSetters(Object beanRequiringInjection) {
		try {
			for (Method method : beanRequiringInjection.getClass().getMethods()) {
				if (method.isAnnotationPresent(InjectMy.class)) {
					List<Object> argumentsToInject = argumentsToInject(method.getParameters());
					method.invoke(beanRequiringInjection, argumentsToInject.toArray());
				}
			}
		} catch (SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new RuntimeException("Error occured while injecting setters", e);
		}
	}

	private <T> void injectFields(T myBean) {
		try {
			for (Field field : myBean.getClass().getDeclaredFields()) {
				if (field.isAnnotationPresent(InjectMy.class)) {
					Object dependency = getBean(field.getType());
					field.setAccessible(true);
					field.set(myBean, dependency);
				}
			}
		} catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
			throw new RuntimeException("Error occured injecting fields", e);
		}

	}

	private <T> T createBeanInstance(Class<T> clazz) {
		try {

			for (Constructor<?> constructor : clazz.getConstructors()) {
				if (constructor.isAnnotationPresent(InjectMy.class)) {
					List<Object> args = argumentsToInject(constructor.getParameters());
					return (T) constructor.newInstance(args.toArray());
				}
			}

			return clazz.newInstance();

		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}

	private List<Object> argumentsToInject(Parameter[] parameters) {
		List<Object> args = new ArrayList<Object>();
		for (Parameter parameter : parameters) {
			args.add(getBean(parameter.getType()));
		}
		return args;
	}
}
