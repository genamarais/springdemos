package com.psybergate.spring.di.demo02.v2.client.config;

import com.psybergate.spring.di.demo02.v2.client.domain.MessageProducer;
import com.psybergate.spring.di.demo02.v2.client.domain.MessageProducerImpl;
import com.psybergate.spring.di.demo02.v2.framework.annotation.Configuration;
import com.psybergate.spring.di.demo02.v2.framework.annotation.MyBean;

@Configuration
public class BeanConfigurations {
	
	@MyBean
	public MessageProducer firstMessageProducer() {
		return new MessageProducerImpl();
	}	

}
