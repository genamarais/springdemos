package com.psybergate.spring.di.demo02.v1.client;

import com.psybergate.spring.di.demo02.v1.framework.Container;

public class MainClient {
	
	public static void main(String[] args) {
		Container container = new Container("demo2-beans.properties");
		MessageRenderer renderer = (MessageRenderer) container.getBean("messageRenderer");
		renderer.render();
	}

}
