package com.psybergate.spring.di.demo01.field.client;

public class MessageRenderer {

	private MessageProducer producer;
	
	public MessageRenderer() {
	}

	public MessageRenderer(MessageProducer producer) {
		this.producer = producer;
	}

	public void render() {
		System.out.println(producer.getMessage());
	}

}
