package com.psybergate.spring.di.demo01.setter.framework;

import java.util.HashMap;
import java.util.Map;

import com.psybergate.spring.di.demo01.setter.client.MessageProducerImpl;
import com.psybergate.spring.di.demo01.setter.client.MessageRenderer;


public class Container {

	Map<String, Object> beans = new HashMap<>();

	public Container() {
		bootstrap();
	}

	private void bootstrap() {
		MessageRenderer renderer = new MessageRenderer();
		renderer.setProducer(new MessageProducerImpl());
		beans.put("messageRenderer", renderer);
	}

	public Object getBean(String beanName) {
		if (!beans.containsKey(beanName)) {
			throw new RuntimeException("Bean not defined");
		}
		return beans.get(beanName);
	}

}
