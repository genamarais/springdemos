package com.psybergate.spring.di.demo01.constructor.client;

public interface MessageProducer {

	String getMessage();

}
