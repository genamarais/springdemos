package com.psybergate.spring.di.demo01.setter.client;

public interface MessageProducer {

	String getMessage();

}
