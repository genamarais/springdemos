package com.psybergate.spring.di.demo01.field.client;

public interface MessageProducer {

	String getMessage();

}
