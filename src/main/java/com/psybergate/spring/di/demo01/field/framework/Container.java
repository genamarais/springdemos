package com.psybergate.spring.di.demo01.field.framework;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import com.psybergate.spring.di.demo01.field.client.MessageProducerImpl;
import com.psybergate.spring.di.demo01.field.client.MessageRenderer;


public class Container {

	Map<String, Object> beans = new HashMap<>();

	public Container() {
		bootstrap();
	}

	private void bootstrap() {
		MessageRenderer renderer = new MessageRenderer();
		injectField(renderer, "producer", new MessageProducerImpl());
		beans.put("messageRenderer", renderer);
	}
	
	private void injectField(Object beanToInject, String fieldName, Object dependecy) {
		try {
			Class<? extends Object> clazz = beanToInject.getClass();
			Field fieldToInject = clazz.getDeclaredField(fieldName);
			fieldToInject.setAccessible(true);
			fieldToInject.set(beanToInject, dependecy);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	public Object getBean(String beanName) {
		if (!beans.containsKey(beanName)) {
			throw new RuntimeException("Bean not defined");
		}
		return beans.get(beanName);
	}

}
