package com.psybergate.spring.di.demo01.constructor.framework;

import java.util.HashMap;
import java.util.Map;

import com.psybergate.spring.di.demo01.constructor.client.MessageProducerImpl;
import com.psybergate.spring.di.demo01.constructor.client.MessageRenderer;


public class Container {

	Map<String, Object> beans = new HashMap<>();

	public Container() {
		bootstrap();
	}

	private void bootstrap() {
		MessageRenderer render = new MessageRenderer(new MessageProducerImpl());
		beans.put("messageRenderer", render);
	}

	public Object getBean(String beanName) {
		if (!beans.containsKey(beanName)) {
			throw new RuntimeException("Bean not defined");
		}
		return beans.get(beanName);
	}

}
