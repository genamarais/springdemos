package com.psybergate.spring.di.demo01.field.client;

import com.psybergate.spring.di.demo01.field.framework.Container;

public class MainClient {
	
	public static void main(String[] args) {
		Container container = new Container();
		MessageRenderer renderer = (MessageRenderer) container.getBean("messageRenderer");
		renderer.render();
	}

}
