package com.psybergate.spring.di.demo01.setter.client;

public class MessageProducerImpl implements MessageProducer {

	private String message = "Hello World!";
	
	public MessageProducerImpl() {
	}
	
	public MessageProducerImpl(String message) {
		this.message = message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Override
	public String getMessage() {
		return message;
	}

}
