package com.psybergate.spring.di.demo01.constructor.client;

import com.psybergate.spring.di.demo01.constructor.framework.Container;

public class MainClient {
	
	public static void main(String[] args) {
		Container container = new Container();
		MessageRenderer renderer = (MessageRenderer) container.getBean("messageRenderer");
		renderer.render();
	}

}
